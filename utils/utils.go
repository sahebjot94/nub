package utils

import (
	"encoding/json"
	"errors"
	"io"
	"os"

	"bitbucket.org/sahebjot94/nub/types"
)

var config *types.Config

func CaptureCrash() {
	if rec := recover(); rec != nil {
		if err, ok := rec.(error); ok {
			handleError(err)
		} else if err, ok := rec.(string); ok {
			handleError(errors.New(err))
		}
	}
}

// can be used to report errors
func handleError(err error) {
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func FetchConfig() *types.Config {
	if config != nil {
		return config
	}
	file, err := os.Open("config.json")
	CheckErr(err)
	defer file.Close()
	config = &types.Config{}
	ParseJson(file, config)
	CheckErr(err)
	return config
}

func ParseJson(stringifiedJson io.Reader, parsedStructVal interface{}) {
	decoder := json.NewDecoder(stringifiedJson)
	err := decoder.Decode(parsedStructVal)
	CheckErr(err)
}
