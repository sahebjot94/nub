package types

import "os"

// A command to execute as spawn/exec
type Cmd struct {
	Name   string
	Args   []string
	Stdin  *os.File
	Stdout *os.File
	Stderr *os.File
}

// Project specific command
type Command struct {
	Name string
	Args []string
}

type Config struct {
	BaseUrl string
}

type ApiResp struct {
	error bool
}

type GitConfig struct {
	Username string
	Email    string
}
