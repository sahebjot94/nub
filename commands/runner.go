package commands

import (
	"os"
	"os/exec"
	"runtime"
	"syscall"

	"bitbucket.org/sahebjot94/nub/git"
	"bitbucket.org/sahebjot94/nub/metric"
	"bitbucket.org/sahebjot94/nub/utils"
)

type Runner struct {
	nubCommands map[string]*Command
}

func NewRunner() *Runner {
	return &Runner{
		nubCommands: make(map[string]*Command),
	}
}

func (r *Runner) Use(command *Command, aliases ...string) {
	r.nubCommands[command.Key] = command
}

func (r *Runner) AllNubCommands() map[string]*Command {
	return r.nubCommands
}

func isBuiltInNubCommand(command string) bool {
	for nubCommand := range CmdRunner.AllNubCommands() {
		if nubCommand == command {
			return true
		}
	}
	return false
}

func (r *Runner) Execute() {
	// Parse the arguments
	args := NewArgs(os.Args[1:])

	if args.Command == "" && len(args.GlobalFlags) == 0 {
		args.Command = "help"
	}

	cmdName := args.Command
	if isBuiltInNubCommand(cmdName) {
		command := CmdRunner.nubCommands[cmdName]
		command.Run()
	} else {
		gitConfig := git.FetchGitConfig()
		if metric.ShouldTrackCommand(cmdName) {
			m := metric.CreateMetric(gitConfig.Username, "commit")
			_ = m
			// m.PushMetrics()
		}

		cmdName := "git"
		cmdArgs := args.FormatArgs()
		argsWithGit := append([]string{cmdName}, cmdArgs...)
		if runtime.GOOS == "windows" {
			cmd := exec.Command(cmdName, cmdArgs...)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			err := cmd.Run()

			if len(os.Args[1:]) != 0 {
				utils.CheckErr(err)
			}
		} else {
			gitBinary, lookErr := exec.LookPath(cmdName)
			utils.CheckErr(lookErr)

			env := os.Environ()
			execErr := syscall.Exec(gitBinary, argsWithGit, env)
			utils.CheckErr(execErr)
		}
	}

}
