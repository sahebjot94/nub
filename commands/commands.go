package commands

var (
	CmdRunner = NewRunner()
)

type Command struct {
	Run   func()
	Key   string
	Usage string
	Long  string
}
