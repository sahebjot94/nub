package commands

type Args struct {
	GlobalFlags []string
	Command     string
	Params      []string
}

func NewArgs(args []string) *Args {
	var (
		command     string
		params      []string
		globalFlags []string
	)

	if len(args) == 0 {
		params = []string{}
	} else {
		command = args[0]
		params = args[1:]
	}

	return &Args{
		GlobalFlags: globalFlags,
		Command:     command,
		Params:      params,
	}
}

func (args *Args) FormatArgs() []string {
	// If command is a git command then return runnabel format
	return append([]string{args.Command}, args.Params...)
}
