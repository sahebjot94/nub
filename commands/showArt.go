package commands

import (
	"fmt"
)

var cmdAsciiArt = &Command{
	Run:   showAsciiArt,
	Key:   "showArt",
	Usage: "showArt",
	Long:  "Shows a dummy ascii art",
}

func init() {
	CmdRunner.Use(cmdAsciiArt)
}

func showAsciiArt() {
	fmt.Println(`
   _____ _ _     _______             _
  / ____(_) |   |__   __|           | |
 | |  __ _| |_     | |_ __ __ _  ___| | __
 | | |_ | | __|    | | '__/ _` + "`" + ` |/ __| |/ /
 | |__| | | |_     | | | | (_| | (__|   <
  \_____|_|\__|    |_|_|  \__,_|\___|_|\_\`)
}
