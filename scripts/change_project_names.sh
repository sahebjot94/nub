#!/bin/sh

function replaceFile() {
  replace="s/$2/$3/"
  sed -e $replace -- $1 > tmp && mv tmp $1
}

function listPath() {
  for file in $1/*
  do
      if [[ -f $file ]]; then
          if [[ $file == *".go"* ]]; then
            replaceFile $file $2 $3
          fi
        else
          listPath $file $2 $3
      fi
  done
}

function listFileAndReplace() {
  listPath $PWD $1 $2
}

listFileAndReplace "gitlab.com" "bitbucket.org"
