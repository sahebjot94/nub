#!/bin/sh
function build(){
  out=$1
  if [ "$1" == "windows" ]; then
    out="$out.exe"
  fi
  env GOOS=$1 GOARCH=$2 go build -o "./dist/nub_$out" .
}

build "darwin" 386
build "linux" "arm"
build "windows" 386

