const http = require('http')
const port = 5000

http.createServer((req, res) => {
  let body = [];
  req.on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();
    console.log(body)
  });

  res.write(JSON.stringify({error: true}));
  res.end()
}).listen(port, () => {
  console.log("Listening on", port)
})


