## Install
  `go get bitbucket.org/sahebjot94/nub`

## Compile
### Windows:
  `scripts\compile`
  This will also create `nub.exe` which can be run directly like this: `nub status`

### Unix based (OSX, Linux)
  `./scripts/compile.sh`

## Run
### Windows
  `scripts\run`
  We can also pass git commands when we run like this:  `scripts\run status`
  
### Unix (OSX, Linux)
  `./scripts/run.sh`
