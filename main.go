package main

import (
	"bitbucket.org/sahebjot94/nub/commands"
	"bitbucket.org/sahebjot94/nub/utils"
)

func main() {

	// global try catch
	defer utils.CaptureCrash()

	config := utils.FetchConfig()
	_ = config

	commands.CmdRunner.Execute()
}
