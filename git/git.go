package git

import (
	"os/exec"
	"strings"

	"bitbucket.org/sahebjot94/nub/types"
	"bitbucket.org/sahebjot94/nub/utils"
)

func RunGitCmd(args []string) ([]byte, error) {
	cmd := exec.Command("git", args...)
	return cmd.CombinedOutput()
}

func FetchGitConfig() *types.GitConfig {
	username, err := RunGitCmd([]string{"config", "user.name"})
	utils.CheckErr(err)
	email, err := RunGitCmd([]string{"config", "user.email"})
	utils.CheckErr(err)
	return &types.GitConfig{configValToString(username), configValToString(email)}
}

func configValToString(b []byte) string {
	return strings.Trim(string(b), "\n")
}
