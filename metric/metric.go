package metric

import (
	"runtime"

	"bitbucket.org/sahebjot94/nub/api"
	"bitbucket.org/sahebjot94/nub/types"
	"bitbucket.org/sahebjot94/nub/utils"
)

var metricCommands = [...]string{"commit"}

func ShouldTrackCommand(cmd string) bool {
	for _, c := range metricCommands {
		if c == cmd {
			return true
		}
	}
	return false
}

// making it like a sum type
// http://www.jerf.org/iri/post/2917
type Metric interface {
	PushMetric()
	// Create(...interface{}) *Metric
}

// TODO: refactor to types itself
type TrackCommand struct {
	Username        string `json:username`
	GitCommand      string `json:git_command`
	OperatingSystem string `json:operating_system`
}

func (t *TrackCommand) PushMetrics() {
	config := utils.FetchConfig()
	api.MakeRequest("POST", config.BaseUrl, t, types.ApiResp{})
}

func CreateMetric(username string, gitCommand string) *TrackCommand {
	osName := runtime.GOOS
	return &TrackCommand{username, gitCommand, osName}
}
