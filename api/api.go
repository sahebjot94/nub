package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/sahebjot94/nub/utils"
)

var client *http.Client

func getHttpClient() *http.Client {
	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}

	if client == nil {
		client = &http.Client{Transport: tr}
	}
	return client
}

func MakeRequest(reqType string, url string, data interface{}, respType interface{}) {
	var req *http.Request
	var err error
	if data != nil {
		j, err := json.Marshal(&data)
		utils.CheckErr(err)
		req, err = http.NewRequest(reqType, url, bytes.NewBuffer(j))
	} else {
		req, err = http.NewRequest(reqType, url, nil)
	}
	utils.CheckErr(err)
	client := getHttpClient()
	resp, err := client.Do(req)
	utils.CheckErr(err)
	defer resp.Body.Close()
	utils.ParseJson(resp.Body, &respType)
}
